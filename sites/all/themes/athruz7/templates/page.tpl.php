<div id="page-wrapper"><div id="page">

  <header id="header"><div class="section clearfix">

    <div id="logo-title">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>

      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan">
          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name">
                <strong>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </strong>
              </div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($site_slogan): ?>
            <div id="site-slogan">
              <?php print $site_slogan; ?>
            </div>
          <?php endif; ?>
        </div> <!-- /#name-and-slogan -->
      <?php endif; ?>
    </div> <!-- /logo-title -->
 
     <?php if (render($page['top_links'])): ?>
       <div id="top-links" class="top-links">
         <?php print render($page['top_links']); ?>
       </div>
    <?php endif; ?>

  </div></header> <!-- /.section, /#header -->



  <?php if ($main_menu): ?>
    <div id="menu-bar" class="clearfix"><div class="section clearfix">
      <nav id="navigation" class="navigation" role="navigation"><div id="main-menu">
        <?php if (module_exists('i18n_menu')) {
            $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
          } else {
            $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
          }
          print drupal_render($main_menu_tree); ?>
      </div></nav> 
    </div></div>
  <?php endif; ?>



  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

    <main id="content" class="column" role="main"><section class="section">
      <a id="main-content"></a>

      <?php if ($page['content_top']): ?>
        <div id="content-top">
          <?php print render($page['content_top']); ?>
        </div> <!--  /#content-top -->
      <?php endif; ?>

      <?php if ($messages): ?>
        <div id="messages"><div class="section clearfix">
          <?php print $messages; ?>
        </div></div> <!-- /.section, /#messages -->
      <?php endif; ?>

      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if (render($tabs)): ?>
        <nav class="tabs" role="navigation">
          <?php print render($tabs); ?>
        </nav>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"> 
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>

      <?php print $feed_icons; ?>
      <?php if ($page['content_bottom']): ?>
        <div id="content-bottom">
          <?php print render($page['content_bottom']); ?>
        </div> <!--  /#content_bottom -->
      <?php endif; ?>

    </section></main> <!-- /.section, /#content -->

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column sidebar"><aside class="section">
        <?php print render($page['sidebar_first']); ?>
      </aside></div> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>
    
    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><aside class="section">
        <?php print render($page['sidebar_second']); ?>
      </aside></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->

  <div id="footer-wrapper"><footer class="section" role="contentinfo">
    <?php if ($page['footer']): ?>
      <div id="footer" role="contentinfo" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>
  </footer></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->
