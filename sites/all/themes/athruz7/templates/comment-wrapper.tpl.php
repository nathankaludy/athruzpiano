<section id="comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php 
	global $user; 
	$userdata = user_load($user->uid);

	if($user->uid)
	{
		if($userdata->picture)
		{
			$user_image = array('style_name' => 'user_simple_image', 'path' => $userdata->picture->uri, );
			print "<div class='comment-user-pic'>".theme('image_style',$user_image)."</div>";
		}
		else
		{
			$user_image = array('style_name' => 'user_simple_image', 'path' => 'public://user-default.png', );
			print "<div class='comment-user-pic'>".theme('image_style',$user_image)."</div>";
		}
	}
?>

  <?php if ($content['comment_form']): ?>
    <?php print render($content['comment_form']); 
    ?>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</section>
