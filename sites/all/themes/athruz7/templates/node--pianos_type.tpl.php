<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>>
        <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
      </h2>
  </header>
    <?php else: ?>
    <?php print render($content);?>
    <?php
      if($node->field_youtube_video) {
	  
	  	print "<div class='utube-lbox'>";
		print "<h3 class='field-label'>"."Video"."</h3>";
		
		for($i=0;$i<=sizeof($node->field_youtube_video);$i++)
		{
			$img_url = $node->field_youtube_video['und'][$i]['video_url'];
			$arr_url = explode('=', $img_url);
			$new_url = "http://www.youtube.com/v/".$arr_url[1];
	
			$img_thumb = $node->field_youtube_video['und'][$i]['thumbnail_path'];
			$img_theme = theme('image_style', array('style_name' => 'piano-thumbnail', 'path' =>$img_thumb));
			
			
			print l($img_theme,$new_url,array('attributes'=>array('rel'=>'lightframe'),'html'=>'true'));
			
		}
		print "</div>";
      } 
  ?>
  <?php endif; ?>

</article>
