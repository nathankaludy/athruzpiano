<html>
<head>
<title>A Thru Z Complete Piano Service</title>
<style>
body {
  padding: 0;
  margin: 0;
  font-family: "Helvetica Neue", helvetica, Arial, sans-serif;
  font-size: 13px;
  line-height: 1.5em;
}
#page {
  font-family: "Helvetica Neue", helvetica, Arial, sans-serif;
  font-size: 16px;
  line-height: 1.5em;
  width: 615px;
  background-color: #ffffff;
  margin: 0 auto;
  min-height: 100%;
  background: #fff;
  -moz-box-shadow: 10px 10px 5px #888;
  -webkit-box-shadow: 10px 10px 5px #888;
  box-shadow: 10px 10px 5px #888;
}
#header {
  position: relative;
  text-align: center;
  background-color: #ffffff;
}

#header a img{
	text-decoration:none;
	outline : none;
	border: 0;
	
	}
.top-links {
  clear: both;
  width: 100%;
  margin: 0;
  background-color: #ffffff;
  text-align: center;
}
.top-links ul, .top-links ul li {
  list-style: none;
  padding: 0;
  margin: 0;
  display: inline;
}
.top-links a.piano {
  display: inline-block;
  height: 34px;
  width: 180px;
}
.top-links a.contact{
  display: inline-block;
  height: 34px;
  width: 150px;
}
#content {
  clear: both;
  overflow: hidden;
  
}
#content .content-inner {
  padding: 20px;
  text-align: left;
}
.footer {
  padding: 15px 0;
  font-size:11px;
  font-weight: bold;
  background: #efefef;
  text-align: center;
  clear: both;
  overflow: hidden;
}
.footer p {
  margin: 0;
}
a, a:link, a:visited {
  color: blue;
  text-decoration: none;
}
a:hover, a:active {
  color: red;
  text-decoration: none;
}
h1, h2, h3, h4, h5, h6 {
  line-height: 1.3em;
}
h1 {
  font-size: 24px;
  padding: 0;
  color: #F00;
  margin: 0 0 25px 0;
}
h2 {
  color: #F00;
  font-size: 1.3em;
  font-weight: bold;
  padding: 0;
  margin: 0 0 15px 0;
}
h3 {
  font-size: 1.4em;
  margin-bottom: 1em;
  color: gray;
  padding: 0;
  margin: 0 0 10px 0;
}
h4 {
  font-size: 1.2em;
  margin-bottom: 0.8em;
}
.img-left, .field-name-field-content-photo {
  float: left;
  margin: 10px 10px 10px 0;
}
.img-right, .field-name-field-content-photo-2 {
  float: right;
  margin: 10px 0 10px 10px;
}
.style1 {color: #000000}
</style>
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="http://piano.nk-dev-server.net/sites/default/files/images/newsletter/background.jpg" bgcolor="#aaaaaa"><tr><td style="padding:20px;">
<div id="page">

<div id="header">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
    <tr><td background="http://piano.nk-dev-server.net/sites/default/files/images/newsletter/header-bg.jpg" align="center" style="background-repeat: no-repeat; background-position: top right; text-align: center; background-color:#ffffff; border:none; text-decoration: none;">

      <div id="logo-title">
       
          <a href="http://athruzpiano.com"><img src="http://piano.nk-dev-server.net/sites/default/files/images/newsletter/logo.png" alt="Home" /></a> </div>
      <div id="top-links" class="top-links">      </div>
<h1 class="style1">Phone: 216-642-9577</h1>
    </td>
    </tr>
  </table>
</div>

<div id="content"> <div class="content-inner">
<h1 class="title"><?php print $title; ?> </h1>
 <?php print render($page['content']); ?>
</div></div>

<div class="footer">
<p>© 2013 A Thru Z's Piano Services</p>
</div>

</div>
</td></tr></table>
</body>
</html>
